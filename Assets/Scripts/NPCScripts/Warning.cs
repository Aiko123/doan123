﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Warning : MonoBehaviour
{
    public GameObject warnSign;
    public TextMeshProUGUI textMeshPro;
    public string[] lines;
    private int index = 0;
    public float textSpeed = 0.1f;
    private bool isTyping = false;
    public bool isDialogueActive = false;

    void Start()
    {
        textMeshPro.text = string.Empty;
        warnSign.SetActive(false); // Ban đầu ẩn dialogue
    }

    void Update()
    {
        if (isTyping == false && warnSign.activeSelf && Input.GetMouseButtonDown(0))
        {
            NextLine();
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Player") && !isDialogueActive)
        {
            // Khi player va chạm, hiển thị dialog và bắt đầu đánh dấu là player gần
            warnSign.SetActive(true);
            StartDialogue();
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // Khi player không còn va chạm nữa, ẩn dialog và đặt index về 0
            warnSign.SetActive(false);
            index = 0;
        }
    }

    void StartDialogue()
    {
        // Bắt đầu hiển thị dialog nếu có dòng nào trong lines
        if (lines.Length > 0)
        {
            isDialogueActive = true;
            StartCoroutine(TypeLine());
        }
    }

    IEnumerator TypeLine()
    {
        isTyping = true;
        foreach (char c in lines[index].ToCharArray())
        {
            textMeshPro.text += c;
            yield return new WaitForSeconds(textSpeed);
        }
        isTyping = false;
    }

    void NextLine()
    {
        // Hiển thị dòng tiếp theo hoặc ẩn dialog nếu đã đến cuối lines
        if (index < lines.Length - 1)
        {
            index++;
            textMeshPro.text = string.Empty;
            StartCoroutine(TypeLine());
        }
        else
        {
            warnSign.SetActive(false);
        }
    }
}
