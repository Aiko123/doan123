using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    private enum LadderPart { complete, top, bot}
    [SerializeField] LadderPart part =LadderPart.complete;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            PlayerController playerController = collision.GetComponent<PlayerController>();
            switch (part)
            {
                case LadderPart.complete:
                    playerController.canClimb = true;
                    break;
                case LadderPart.top:
                    //playerController.isTop = true;
                    break;
                case LadderPart.bot:
                    break;
                default:
                    break;

            }
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            PlayerController playerController = collision.GetComponent<PlayerController>();
            switch (part)
            {
                case LadderPart.complete:
                    playerController.canClimb = false;
                    break;
                case LadderPart.top:
                    //playerController.isTop = true;
                    break;
                case LadderPart.bot:
                    break;
                default:
                    break;

            }
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
