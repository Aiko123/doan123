﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpossumMove : Destroyed
{
    public float moveSpeed = 2.0f;
    public float moveRange = 4.0f;
    private Vector3 originalPosition;
    private bool movingRight = true;

    void Start()
    {
        originalPosition = transform.position;
    }

    void Update()
    {
        float targetPositionX = movingRight ? originalPosition.x + moveRange : originalPosition.x - moveRange;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(targetPositionX, transform.position.y, transform.position.z), moveSpeed * Time.deltaTime);

        if (transform.position.x == targetPositionX)
        {
            movingRight = !movingRight;
        }

        if (movingRight)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
    }
}
