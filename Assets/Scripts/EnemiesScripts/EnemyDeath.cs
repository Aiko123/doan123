﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rb;


    public void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !animator.GetBool("Death")&& (rb.velocity.y > 0))
        {

            
            {
                animator.SetBool("Death", true);
            }
            Destroy(this.gameObject);

        }
       
    }
  
}
