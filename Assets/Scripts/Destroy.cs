using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyed : MonoBehaviour
{
    public void Delete()
    {

        Destroy(this.gameObject);
    }
}
