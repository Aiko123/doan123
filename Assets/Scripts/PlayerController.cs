﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public float speed = 7f;
    public float jumpForce = 10f;
    public Rigidbody2D rb;
    private RigidbodyConstraints2D originalConstraints;
    public Animator animator;
    public GameManager gameManager;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public LayerMask ladderLayer;
    public AudioClip jumpClip;
    public AudioClip hurtClip;
    float hurtTime = 0.5f;
    float currentTime = 0f;
    float nextTime = 0f;
    float move;
    private bool isGrounded = false;
    public bool isClimbing = false;
    public bool isTop = false;
    public bool canClimb = false;
    public Warning warnSign;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        originalConstraints = rb.constraints;
    }
    void Update()
    {
        move = Input.GetAxis("Horizontal");

        Movement();
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Jump();
        }
        Climb();
        UpdateAnimation();
        if (warnSign.GetComponent<Warning>().isDialogueActive)
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            return;
        }
        else
        {
            rb.constraints = originalConstraints;
        }
    }
    private void Movement()
    {
        if (canClimb&& Mathf.Abs(Input.GetAxis("Vertical"))>0.1f)
        {
            Climb();
        }
        rb.velocity = new Vector2(speed * move, rb.velocity.y);
        if (move != 0)
        {
            transform.localScale = new Vector3(Mathf.Sign(move) * 4, 4, 4);
        }
        animator.SetBool("Running", Mathf.Abs(move) > 0.1f);
    }
    private void Jump()
    {

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            gameManager.audioManager.PlayOneShot(jumpClip, 2f);

            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            if (Mathf.Abs(move) > 0f)
            {
                animator.SetBool("Running", false);
                animator.SetBool("JumpUp", true);
            }
        }
    }
    private void UpdateAnimation()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
        animator.SetBool("JumpUp", !isGrounded && rb.velocity.y > 0);
        animator.SetBool("FallingDown", !isGrounded);
        animator.SetBool("Running", Mathf.Abs(move) > 0f);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (rb.velocity.y < 0)
            {
                gameManager.audioManager.PlayOneShot(jumpClip, 2f);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce * 0.5f);
                collision.gameObject.GetComponent<Destroyed>().Delete();
            }
            else
            {
                gameManager.audioManager.PlayOneShot(hurtClip, 2f);
                if (collision.transform.position.x >= transform.position.x)
                {
                    if (currentTime >= nextTime)
                    {
                        nextTime += hurtTime;
                        rb.velocity = new Vector2(-1f * speed * Time.fixedDeltaTime, rb.velocity.y);
                        animator.SetTrigger("Hurt");
                    }
                }
                else
                {
                    if (currentTime >= nextTime)
                    {
                        nextTime += hurtTime;
                        rb.velocity = new Vector2(1f * speed * Time.fixedDeltaTime, rb.velocity.y);
                        animator.SetTrigger("Hurt");
                    }
                }
            }
        }
        if (collision.gameObject.layer == ladderLayer)
        {
            canClimb = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == ladderLayer)
        {
            canClimb = false;
        }
    }

    private void Climb()
    {
        isClimbing = (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S)) && canClimb;
        animator.SetBool("Climbing", isClimbing);

        if (isClimbing)
        {

            AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(0);
            float normalizedTime = currentState.normalizedTime;

            if (normalizedTime < 1f / 3f)
            {
            }
            else if (normalizedTime < 2f / 3f)
            {
            }
             else
            {
            }
        }
    }


}

