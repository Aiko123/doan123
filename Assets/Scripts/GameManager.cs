using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public AudioSource audioManager;
    public Animator fadeAnim;


    public void ChangeScene(int index)
    {
        fadeAnim.SetTrigger("FadeOut");
    }

   
}
